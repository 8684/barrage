//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    appBgColor: '#172c3d',
    appColor: '#ffffff',
    textShadow: false,
    shanshuo: false,
    fontSize: 80,
    step: 4,
    show: false,
    inputText: '唱的真好听...',
    controls: [
      {
        title: "滚动速度",
        content: [
          {
            text: "快",
            val: 12,
            select:false
          },
          {
            text: "正常",
            val: 6,
            select: false
          },
          {
            text: "慢",
            val: 3,
            select: false
          }
        ]
      },
      {
        title: "字体大小",
        content: [
          {
            text: "超大",
            val: 100
          },
          {
            text: "大",
            val: 80
          },
          {
            text: "正常",
            val: 60
          },
          {
            text: "小",
            val: 40
          }
        ]
      }
      ,
      {
        title: "字体特效",
        content: [
          {
            text: "阴影",
            bool: false
          },
          {
            text: "闪烁",
            bool: false
          }
        ]
      },
       {
        title: "字体颜色",
         colors: ["#ffffff", "#ff0000", "#172c3d", "#264863", "#00ff03", "#aedd81"]
      }, {
        title: "屏幕背景",
         colors: ["#172c3d", "#264863", "#014d67", "#aedd81", "#ffffff","#d971d4"]
      }
      
      
    ]
  },
  onLoad: function () {
    wx.loadFontFace({
      family: 'SourceHanSansCN',
      source: 'url("http://abc.bimcn.cc/maxwx/plug-in/weixin/font/SourceHanSansCN-Regular.ttf")',
      success: console.log
    })
    wx.showNavigationBarLoading();
    setTimeout(function () { wx.hideNavigationBarLoading(); }, 3000);


    let spW = 0;
    //创建节点选择器
    var query = wx.createSelectorQuery();
    query.select('#sp').boundingClientRect()
    query.exec(function (res) {
      //res就是 所有标签为sp的元素的信息 的数组
      console.log(res);
      //取高度
      console.log(res[0].width);
      spW = res[0].width;
    })


    let wH = wx.getSystemInfoSync().windowHeight;
    this.setData({
      contentW: wH + spW,
      danmuL: wH + spW
    })

    setInterval(() => {
      let step = this.data.step;
      let dl = this.data.danmuL - step;
      if (dl < -(wH + spW)) {
        dl = this.data.contentW
      }
      this.setData({
        danmuL: dl
      })

    }, 20);

  },
  bi(e) {
    console.log(e.detail.value)
    this.setData({
      inputText: e.detail.value
    })
  },
  clickShowHide() {
    let s = !this.data.show
    this.setData({
      show: s
    })
  },
  checkAppBgColor(e) {
    var that=this;
    let idx = e.currentTarget.dataset.idx;
    let idxx = e.currentTarget.dataset.idxx;
    let data = that.data;
    if (idx == 4) {
      that.setData({
        appBgColor: data.controls[idx].colors[idxx]
      })

    }
    else if (idx == 3) {

      that.setData({
        appColor: data.controls[idx].colors[idxx]
      })
      
    }
    else{
      data.controls[idx].content[idxx].select = true;
      var objces = data.controls[idx].content;
      objces.forEach(function (value, index, arrSelf) {
        if (idxx != index){
          data.controls[idx].content[index].select=false;
        }

      });

      switch(idx){
        case 0:
          
          that.setData({
            step: data.controls[idx].content[idxx].val
          })
        break;
        case 1:
          that.setData({
            fontSize: data.controls[idx].content[idxx].val
          })
          break;
        case 2:
          
          
          data.controls[idx].content[idxx].val = !data.controls[idx].content[idxx].val
          if (idxx == 0) {
            that.setData({
              controls: data.controls,
              textShadow: data.controls[idx].content[idxx].val
            })
          } else if (idxx == 1) {
            that.setData({
              controls: data.controls,
              shanshuo: data.controls[idx].content[idxx].val
            })
          }
          break;
      };

      that.setData({
        controls: data.controls,
      })
    }

  },

  //字号
  pickerChange1(e) {
    let idx2 = e.detail.value;
    console.log("idx2->", idx2)
    console.log("this.data.controls2[0].array[idx2]-->", this.data.controls2[0].array[idx2])
    this.setData({
      fontSize: this.data.controls2[0].array[idx2],
      index: e.detail.value
    })
  },
  //速度
  pickerChange2(e) {
    let idx2 = e.detail.value;
    console.log("idx2->", idx2)
    console.log("this.data.controls2[1].array[idx2]-->", this.data.controls2[1].array[idx2])
    this.setData({
      step: this.data.controls2[1].array[idx2],
      index2: e.detail.value
    })
  }
})
